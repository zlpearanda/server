<?php
//Make sure that it is a POST request.
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
    throw new Exception('Request method must be POST!');
}
else{
  require_once 'connection.php';
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if(strcasecmp($contentType, 'application/json') != 0){
    throw new Exception('Content type must be: application/json');
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

//If json_decode failed, the JSON is invalid.
if(!is_array($decoded)){
    throw new Exception('Received content contained invalid JSON!');
}

  global $connect;
  $id = $decoded['user_ID'];
  $password= $decoded['Password'];
  

  // echo "$area,$building,$floor,$roomName";
  $query = "Select ID_Number,Password From Users Where ID_Number = '$id' and Password = '$password';";
  $result=mysqli_query($connect,$query) or die(mysqli_error($connect));
 
  if(mysqli_num_rows($result)>0){
    echo "Success";
  }else{
    echo "Failed";
  }

    
   mysqli_close($connect);
?>
