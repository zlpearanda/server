<?php
echo "signals.php";
//Make sure that it is a POST request.
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
    throw new Exception('Request method must be POST!');
}
else{
	// require_once 'connection.php';
	require_once 'modeConnection.php';
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if(strcasecmp($contentType, 'application/json') != 0){
    throw new Exception('Content type must be: application/json');
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);
 
//If json_decode failed, the JSON is invalid.
if(!is_array($decoded)){
    throw new Exception('Received content contained invalid JSON!');
}
	global $connect2;

	// var_dump($decoded);
	$area = $decoded['Area'];
	$building= $decoded['Building'];
	$floor= $decoded['Floor'];
	$roomName= $decoded['Room'];
	$signals = $decoded['signals'];
		// check if info exist
	$query = "Insert ignore into Rooms(Area,Building,FloorNum,RoomName) values ('$area','$building','$floor','$roomName');";
	// mysqli_query($connect,$query) or die(mysqli_error($connect));
	
	if (mysqli_query($connect2, $query)) {
		
		$last_idMd = mysqli_insert_id($connect2);
		// $last_idMd = mysqli_insert_id($connect2);
 		// echo $last_idMn." ".$last_idMd;
    	if($last_idMd!=0 ){

    	echo "New record created successfully. Last inserted ID is: " . $last_idMd;
    	$currentGrid=-1;
		$Array=[];
		$rssArray=array();

		foreach ($signals as $rss) {
			foreach ($rss as $data) {
				$name = $data['SSID'];
				$MAC =  $data['BSSID'];
				$GridNumber = $data['GridNumber'];
				$RSS = $data['RSS'];
					
				if ($currentGrid == $GridNumber) {
					if (isAvailable($MAC,$Array)) {
						foreach ($Array as $value) {
							$temp = $value->{'BSSID'};
							if($MAC == $temp){	
								$value->setRSS($RSS);
							}
						}
							
					}else{
						$rssArray=[];
						array_push($rssArray,$RSS);
						$Array[]= new GridData($name,$MAC,$rssArray);
					}
				}else{
			   		if($currentGrid!=-1){
						$query2 = "Insert ignore into Grid(Room_ID,GridNumber) values ('$last_idMd','$currentGrid');";
						// $resultMn= mysqli_query($connect, $query2);
						$resultMd= mysqli_query($connect2, $query2);

						if(mysqli_insert_id($connect2) !=0 ){
							// $grid_idMn = mysqli_insert_id($connect);
							$grid_idMd = mysqli_insert_id($connect2);
						}
						if ($resultMd) {
							foreach ($Array as $value) {
								$MAC_Address = $value->{'BSSID'};
								$rssVal=$value->{'RssVal'};
								$AP=$value->{'SSID'};
								//for mean
								$count = count($rssVal); 
								$sum = array_sum($rssVal); 
								$mean = $sum/$count;
								//for mode
								$v = array_count_values($rssVal);
								arsort($v); 
								foreach($v as $k => $v){$mode = $k; break;} 

								// $queryMn = "Insert into RSSi(MAC_Address,RSSI,LocationID) values ('$MAC_Address','$mean','$grid_idMd');";
								// mysqli_query($connect, $queryMn);
								$queryMd = "Insert into RSSi(MAC_Address,RSSI,LocationID) values ('$MAC_Address','$mode','$grid_idMd');";
								mysqli_query($connect2, $queryMd);
								$query4 = "Insert ignore into Access_Points(MAC_Address,SSID) values ('$MAC_Address','$AP');";
								// mysqli_query($connect, $query4);
								mysqli_query($connect2, $query4);
							}
						 
						 
						}
					}
					$currentGrid=$GridNumber;
					$Array=[];
					$rssArray=[];
					array_push($rssArray,$RSS);
					$Array[] = new GridData($name,$MAC,$rssArray);
				}
			}
		}
		$query2 = "Insert ignore into Grid(Room_ID,GridNumber) values ('$last_idMd','$currentGrid');";
						// $resultMn= mysqli_query($connect, $query2);
						$resultMd= mysqli_query($connect2, $query2);

						if(mysqli_insert_id($connect2) !=0 ){

							// $grid_idMn = mysqli_insert_id($connect);
							$grid_idMd = mysqli_insert_id($connect2);
							echo $grid_idMd."\n";
						}
						if ($resultMd) {
							foreach ($Array as $value) {
								$MAC_Address = $value->{'BSSID'};
								$rssVal=$value->{'RssVal'};
								$AP=$value->{'SSID'};
								//for mean
								$count = count($rssVal); 
								$sum = array_sum($rssVal); 
								$mean = $sum/$count;
								//for mode
								$v = array_count_values($rssVal);
								arsort($v); 
								foreach($v as $k => $v){$mode = $k; break;} 

								// $queryMn = "Insert into RSSi(MAC_Address,RSSI,LocationID) values ('$MAC_Address','$mean','$grid_idMn');";
								// mysqli_query($connect, $queryMn);
								$queryMd = "Insert into RSSi(MAC_Address,RSSI,LocationID) values ('$MAC_Address','$mode','$grid_idMd');";
								mysqli_query($connect2, $queryMd);
								$query4 = "Insert ignore into Access_Points(MAC_Address,SSID) values ('$MAC_Address','$AP');";
								// mysqli_query($connect, $query4);
								mysqli_query($connect2, $query4);
							}
						 
						 
						}
		}
    }else {
    	echo "Error: " . $sql . "<br>" . mysqli_error($connect2);
	}
	
	mysqli_close($connect2);
	
/**
 * 
 */
function isAvailable($address,$Array){
	foreach ($Array as $value) {
		
		if($address == $value->{'BSSID'}){
			return true;
		}
		
	}
	return false;
}
class GridData
{
	public $SSID;
	public $BSSID;
	public $RssVal=array();

	function __construct($SSID,$BSSID,array $RssVal){
		
		$this->SSID = $SSID;
		$this->BSSID = $BSSID;
		$this->RssVal = $RssVal;
	}

	function setRSS($value)
	{
		array_push($this->RssVal,$value);
	}
	function getRSS(){
		return $this->RssVal;
	}

}

?>
