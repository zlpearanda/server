<?php
echo "signals.php";
//Make sure that it is a POST request.
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
    throw new Exception('Request method must be POST!');
}
else{
	require_once 'modeConnection.php';
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if(strcasecmp($contentType, 'application/json') != 0){
    throw new Exception('Content type must be: application/json');
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);
 
//If json_decode failed, the JSON is invalid.
if(!is_array($decoded)){
    throw new Exception('Received content contained invalid JSON!');
}
	global $connect2;
	// var_dump($decoded);
	$area = $decoded['Area'];
	$building= $decoded['Building'];
	$floor= $decoded['Floor'];
	$roomName= $decoded['Room'];
	$signals = $decoded['signals'];
		// check if info exist
	$query = "Insert ignore into Rooms(Area,Building,FloorNum,RoomName) values ('$area','$building','$floor','$roomName');";
	// mysqli_query($connect,$query) or die(mysqli_error($connect));
	
	if (mysqli_query($connect2, $query)) {
    	$last_id = mysqli_insert_id($connect2);
 		
    	if($last_id!=0){

    	echo "New record created successfully. Last inserted ID is: " . $last_id;
    		$currentGrid=0;
			$Array=[];
			$rssArray = [];
			foreach ($signals as $rss) {
				foreach ($rss as $data) {
					$name = $data['SSID'];
					$MAC =  $data['BSSID'];
					$GridNumber = $data['GridNumber'];
					$RSS = $data['RSS'];
					
					if ($currentGrid == $GridNumber) {

						if (isAvailable($MAC,$Array)) {

							foreach ($Array as $value) {
								$temp = $value->{'BSSID'};
								
								if($MAC == $temp){	
									$value->setRSS($RSS);
								}
							}
							
						}else{

							$Array[]= new GridData($name,$MAC,$rssArray);
						}
					}else{
						// var_dump($Array);
					echo "new grid";
			   			$query2 = "Insert ignore into Grid(Room_ID,GridNumber) values ('$last_id','$currentGrid');";
			   			$result= mysqli_query($connect2, $query2);
			   			if(mysqli_insert_id($connect2) !=0){
			   				$grid_id = mysqli_insert_id($connect2);
			   			}
			   			if ($result) {
			   				
			   				foreach ($Array as $value) {
			   					echo "\n new array \n";
			   					$MAC_Address = $value->{'BSSID'};
			   					$RSS=$value->{'RssVal'};
			   					$AP=$value->{'SSID'};
			   					
			   					$v = array_count_values($RSS);

			   				arsort($v); 
   							foreach($v as $k => $v){$total = $k; break;} 
				
			   					$query3 = "Insert into RSSi(MAC_Address,RSSI,LocationID) values ('$MAC_Address','$total','$grid_id');";
			   					echo $query3;
			   					echo "\n";
					    		mysqli_query($connect2, $query3);
					    		$query4 = "Insert ignore into Access_Points(MAC_Address,SSID) values ('$MAC_Address','$AP');";
				    			mysqli_query($connect2, $query4);
				    			echo $query4;
			   				}
				    	
				    	
						}
						$currentGrid=$GridNumber;
						$Array=[];
						$rssArray = [];
						$Array[] = new GridData($name,$MAC,$rssArray);
					}
				}
			}
			$query2 = "Insert ignore into Grid(Room_ID,GridNumber) values ('$last_id','$currentGrid');";
			   			$result= mysqli_query($connect2, $query2);
			   			if(mysqli_insert_id($connect2) !=0){
			   				$grid_id = mysqli_insert_id($connect2);
			   			}
			   			if ($result) {
			   				
			   				foreach ($Array as $value) {
			   					$MAC_Address = $value->{'BSSID'};
			   					$RSS=$value->{'RssVal'};
			   					$AP=$value->{'SSID'};

			   					$v = array_count_values($RSS); 
								arsort($v); 
   								foreach($v as $k => $v){$total = $k; break;} 
			   				
			   					$query3 = "Insert into RSSi(MAC_Address,RSSI,LocationID) values ('$MAC_Address','$total','$grid_id');";
			   					echo $query3;
			   					echo "\n";
					    		mysqli_query($connect2, $query3);
					    		$query4 = "Insert ignore into Access_Points(MAC_Address,SSID) values ('$MAC_Address','$AP');";
				    			mysqli_query($connect2, $query4);
				    			echo $query4;
			   				}
				    	
				    	
						}
    	}
    }else {
    	echo "Error: " . $sql . "<br>" . mysqli_error($connect2);
	}
	
	mysqli_close($connect2);
	
/**
 * 
 */
function isAvailable($address,$Array){
	foreach ($Array as $value) {
		
		if($address == $value->{'BSSID'}){
			return true;
		}
		
	}
	return false;
}
class GridData
{
	public $SSID;
	public $BSSID;
	public $RssVal=array();
	// public $frequenncy;
	function __construct($SSID,$BSSID,array $RssVal){
		
		$this->SSID = $SSID;
		$this->BSSID = $BSSID;
		$this->RssVal = $RssVal;
		// $this->frequenncy=$frequenncy;
	}
	function setRSS($value)
	{
		array_push($this->RssVal, $value);
	}
	function getRSS(){
		return $this->RssVal;
	}

}

?>
