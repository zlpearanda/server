<?php
require_once 'connection.php';

global $connect;


$str = file_get_contents("lec1.json");
$json = json_decode($str,true); 
// var_dump($json);

$currentGrid=-1;
$Array=[];
$rssArray=array();
$RssFinal=[];
	
foreach ($json as $rss) {
	foreach ($rss as $data) {
		$name = $data['SSID'];
		$MAC =  $data['BSSID'];
		$GridNumber = $data['GridNumber'];
		$RSS = $data['RSS'];
		
		if($GridNumber == $currentGrid){
			if (isAvailable($MAC,$Array)) {
				foreach ($Array as $value) {
					$temp = $value->{'BSSID'};
					if($MAC == $temp){	
						$value->setRSS($RSS);
					}
				}		
			}else{
				$rssArray=[];
				array_push($rssArray,$RSS);
				$Array[]= new GridData($name,$MAC,$rssArray);
			}
		}else{
			
			if($currentGrid!=-1){
				echo $currentGrid;
				foreach ($Array as $value) {
					$MAC_Address = $value->{'BSSID'};
					$rssVal=$value->{'RssVal'};
					$v = array_count_values($rssVal); 
	
					//for mode
					// arsort($v); 
					// foreach($v as $k => $v){$mode = $k; break;} 
					//for mean   
					$count = count($rssVal); 
					$sum = array_sum($rssVal); 
					$mean = $sum/$count;
					$RssFinal[] = new SignalVal($mean,$MAC_Address);
	
				}	
				$signals=sortRSS($RssFinal,$connect);
				$LocID=SearchSpace($signals,$connect);
				computeEuclidean($signals,$LocID,$connect);
				echo "<br>";
			}
			
			$currentGrid=$GridNumber;
			$Array=[];
			$rssArray = [];
			$RssFinal = [];
			array_push($rssArray,$RSS);
			$Array[] = new GridData($name,$MAC,$rssArray);
		}
	}
}
echo $currentGrid;
				foreach ($Array as $value) {
					$MAC_Address = $value->{'BSSID'};
					$rssVal=$value->{'RssVal'};
					$v = array_count_values($rssVal); 
	
					//for mode
					// arsort($v); 
					// foreach($v as $k => $v){$mode = $k; break;} 
					//for mean   
					$count = count($rssVal); 
					$sum = array_sum($rssVal); 
					$mean = $sum/$count;
					$RssFinal[] = new SignalVal($mean,$MAC_Address);
	
				}	
				$signals=sortRSS($RssFinal,$connect);
				$LocID=SearchSpace($signals,$connect);
				computeEuclidean($signals,$LocID,$connect);
				echo "<br>";



function cmp($a, $b)
	{
		return $a->RssVal < $b->RssVal;
	}
function sortRSS($Rss,$connect){
		
		foreach ($Rss as $RSS) {
		$RSSi = $RSS->{'RssVal'}; 
		$mac = $RSS->{'B'};
		// echo $mac;
		$q = "SELECT MAC_ADDRESS from RSSi WHERE MAC_ADDRESS = '$mac';";
		$res = mysqli_query($connect,$q);
		if(mysqli_num_rows($res)!=0){
			// echo $RSSi;
			$arraySignals[] = new SignalVal($RSSi,$mac);
		}	
		
	}
	
	usort($arraySignals, "cmp");
	return $arraySignals;
}
function SearchSpace($arraySignals,$connect){
	$counter=0;
	$macAddresses="";
	foreach ($arraySignals as $key ) {
		$value=$key->{'B'};
		if($counter<3){
			$macAddresses.="'$value',";
		}
		$counter+=1;
	}
	
	$macAddresses = substr_replace($macAddresses, "", -1);
	
	//selecting search space
	$locationQuery = "SELECT LocationID FROM `RSSi` WHERE MAC_Address IN ($macAddresses) GROUP by LocationID HAVING COUNT(*)>1;";
	$locationIDs = mysqli_query($connect,$locationQuery);
	$LID="";
	if($locationIDs){
		while ($row = mysqli_fetch_assoc($locationIDs)) {
			
			$grids = $row['LocationID'];
			$LID .="'$grids',";
		}
	}
	$LID = substr_replace($LID, "", -1);
	return $LID;
}
function findRSS($MAC,$arraySignals){
	$result= -100;
	foreach ($arraySignals as $object) {
		if($MAC == $object->{'B'}){
			
			$result = (int)$object->{'RssVal'};
		}
	}
	return $result;
}
function eCmp($a, $b)
{
    return $a->EuclideanDist > $b->EuclideanDist;
}
function computeEuclidean($arraySignals,$LID,$connect){
	$searchSpaceQuery = "SELECT * FROM `RSSi` WHERE LocationID IN ($LID);";
	$searchSpace = mysqli_query($connect,$searchSpaceQuery);
	$locID = 0;
	$distance = 0;
	$eDistances=[];
	
	if($searchSpace){
	
		while ($resultRow = mysqli_fetch_assoc($searchSpace)) {
			$ID = $resultRow['LocationID'];
			$rssI = $resultRow['RSSI'];
			$macAdd = $resultRow['MAC_Address'];
			$val =findRSS($macAdd,$arraySignals);
			$base = ($rssI)-($val);
			
			if($locID == $ID ){		
				$distance += pow($base, 2);
			}elseif($locID !== (int)$ID&&$locID!=0){
				$eDistances[] = new EuclideanDistance(sqrt(abs($distance)),$locID);
				$locID = $ID;
				$distance = pow($base, 2);
				
			}elseif ($locID==0) {
				$locID = $ID;
			}
		}
		$eDistances[] = new EuclideanDistance(sqrt(abs($distance)),$locID);
	}
	usort($eDistances, "eCmp");
	$Grid = $eDistances[0]->{'ReferencePoint'};
	var_dump($Grid);
	$queryLocation = "SELECT GridNumber,Room_ID FROM Grid WHERE Grid_ID = $Grid";
	$EstimatedLocation = mysqli_query($connect,$queryLocation);
	if($EstimatedLocation){
		while ($row = mysqli_fetch_assoc($EstimatedLocation)) {
			
			$grid = $row['GridNumber'];
			$room = $row['Room_ID'];
		}
	}
	$queryRoom = "SELECT * FROM Rooms WHERE ID = $room";
	$EstimatedRoom = mysqli_query($connect,$queryRoom);
	if($EstimatedRoom){
		while($row = mysqli_fetch_assoc($EstimatedRoom)){
			$eLocation= "Estimated Location is at ".$row['Area']." , ".$row['Building']." , ".$row['RoomName']." Grid Number ".$grid;
		}		
	}
	echo $eLocation;
}
class SignalVal{
	public $RssVal;
	public $B;
	
	function __construct($RssVal,$B){
		$this->RssVal = $RssVal;
		$this->B = $B;
	}
}
function isAvailable($address,$Array){
	foreach ($Array as $value) {
		
		if($address == $value->{'BSSID'}){
			return true;
		}
		
	}
	return false;
}
class GridData
{
	public $SSID;
	public $BSSID;
	public $RssVal=array();
	// public $frequenncy;
	function __construct($SSID,$BSSID,array $RssVal){
		
		$this->SSID = $SSID;
		$this->BSSID = $BSSID;
		$this->RssVal = $RssVal;
		// $this->frequenncy=$frequency;
	}
	function setRSS($value)
	{
		array_push($this->RssVal, $value);
	}
	function getRSS(){
		return $this->RssVal;
	}

}
class EuclideanDistance
{
	public $EuclideanDist;
	public $ReferencePoint;
	
	function __construct($EuclideanDist,$ReferencePoint)
	{
		$this->EuclideanDist = $EuclideanDist;
		$this->ReferencePoint = $ReferencePoint;
		
	}
}
?>