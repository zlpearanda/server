<?php
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
    throw new Exception('Request method must be POST!');
}
else{
	require_once 'connection.php';
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if(strcasecmp($contentType, 'application/json') != 0){
    throw new Exception('Content type must be: application/json');
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);
 
//If json_decode failed, the JSON is invalid.
if(!is_array($decoded)){
    throw new Exception('Received content contained invalid JSON!');
}


$signals = $decoded['Signals'];

$currentGrid=0;
$Array=[];
$arraySignals=[];
$RssMean=[];
foreach ($signals as $rss) {
	foreach ($rss as $data) {
		$name = $data['SSID'];
		$MAC =  $data['BSSID'];
		$GridNumber = $data['GridNumber'];
		$RSS = $data['RSS'];
		
		if (isAvailable($MAC,$Array)) {
			foreach ($Array as $value) {
				$temp = $value->{'BSSID'};
				if($MAC == $temp){	
					$value->setFrequency(($value->getFrequency())+1);
					$value->setRSS(($value->getRSS())+($RSS));
				}
			}		
		}else{
			$Array[]= new GridData($name,$MAC,$RSS,1);
		}
	}
}

foreach ($Array as $value) {
	$MAC_Address = $value->{'BSSID'};
	$RSS=$value->{'RssVal'}/$value->{'frequency'};
	$RssMean[] = new SignalVal($RSS,$MAC_Address);
}

function isAvailable($address,$Array){
	foreach ($Array as $value) {
		
		if($address == $value->{'BSSID'}){
			return true;
		}
	}
	return false;
}
class GridData
{
	public $SSID;
	public $BSSID;
	public $RssVal;
	public $frequenncy;
	function __construct($SSID,$BSSID,$RssVal,$frequency){
		
		$this->SSID = $SSID;
		$this->BSSID = $BSSID;
		$this->RssVal = $RssVal;
		$this->frequency=$frequency;
	}
	function setFrequency($value)
	{
		$this->frequency=$value;
	}
	function getFrequency(){
		return $this->frequency;
	}
	function setRSS($value)
	{
		$this->RssVal=$value;
	}
	function getRSS(){
		return $this->RssVal;
	}

}


	foreach ($RssMean as $RSS) {
		$RSSi = $RSS->{'RssVal'}; 
		$mac = $RSS->{'B'};
		// echo $mac;
		$q = "SELECT MAC_ADDRESS from RSSi WHERE MAC_ADDRESS = '$mac';";
		$res = mysqli_query($connect,$q);
		if(mysqli_num_rows($res)!=0){
			// echo $RSSi;
			$arraySignals[] = new SignalVal($RSSi,$mac);
		}	
	}
	
function cmp($a, $b)
{
    return $a->RssVal < $b->RssVal;
}

usort($arraySignals, "cmp");
// // var_dump($arraySignals);
$counter=0;
foreach ($arraySignals as $key ) {
	$value=$key->{'B'};
	if($counter<3){
		$macAddresses.="'$value',";
	}
	$counter+=1;
}

$macAddresses = substr_replace($macAddresses, "", -1);

//selecting search space
$locationQuery = "SELECT LocationID FROM `RSSi` WHERE MAC_Address IN ($macAddresses) GROUP by LocationID HAVING COUNT(*)=3;";
$locationIDs = mysqli_query($connect,$locationQuery);
$LID="";
if($locationIDs){
	while ($row = mysqli_fetch_assoc($locationIDs)) {
		
		$grids = $row['LocationID'];
		$LID .="'$grids',";
	}
}

// // var_dump($arraySignals);
function findRSS($MAC,$arraySignals){
	$result= -100;
	foreach ($arraySignals as $object) {
		if($MAC == $object->{'B'}){
			
			$result = (int)$object->{'RssVal'};
		}
	}
	return $result;
}
$LID = substr_replace($LID, "", -1);
$searchSpaceQuery = "SELECT * FROM `RSSi` WHERE LocationID IN ($LID);";
$searchSpace = mysqli_query($connect,$searchSpaceQuery);
$locID = 0;
$distance = 0;
$eDistances=[];

if($searchSpace){

	while ($resultRow = mysqli_fetch_assoc($searchSpace)) {
		$ID = $resultRow['LocationID'];
		$rssI = $resultRow['RSSI'];
		$macAdd = $resultRow['MAC_Address'];
		$val =findRSS($macAdd,$arraySignals);
		$base = ($rssI)-($val);
		
		if($locID == $ID ){		
			$distance += pow($base, 2);
		}elseif($locID !== (int)$ID&&$locID!=0){
			$eDistances[] = new EuclideanDistance(sqrt(abs($distance)),$locID);
			$locID = $ID;
			$distance = pow($base, 2);
			
		}elseif ($locID==0) {
			$locID = $ID;
		}
	}
	$eDistances[] = new EuclideanDistance(sqrt(abs($distance)),$locID);
}

class SignalVal{
	public $RssVal;
	public $B;
	
	function __construct($RssVal,$B){
		$this->RssVal = $RssVal;
		$this->B = $B;
	}
}
class EuclideanDistance
{
	public $EuclideanDist;
	public $ReferencePoint;
	
	function __construct($EuclideanDist,$ReferencePoint)
	{
		$this->EuclideanDist = $EuclideanDist;
		$this->ReferencePoint = $ReferencePoint;
		
	}
}
function eCmp($a, $b)
{
    return $a->EuclideanDist > $b->EuclideanDist;
}

usort($eDistances, "eCmp");
// foreach ($eDistances as $object) {
// 		echo "ReferencePoint: ";
// 		echo $object->{'ReferencePoint'};
// 		echo "\n";
// 		echo "Distance ";
// 		echo $object->{'EuclideanDist'};
// 		echo "\n";
		
// }	
$Grid = $eDistances[0]->{'ReferencePoint'};
$queryLocation = "SELECT GridNumber,Room_ID FROM Grid WHERE Grid_ID = $Grid";
$EstimatedLocation = mysqli_query($connect,$queryLocation);

if($EstimatedLocation){
	while ($row = mysqli_fetch_assoc($EstimatedLocation)) {
		
		$grid = $row['GridNumber'];
		$room = $row['Room_ID'];
	}
}
$queryRoom = "SELECT * FROM Rooms WHERE ID = $room";
$EstimatedRoom = mysqli_query($connect,$queryRoom);
$UserID = $decoded['UserID'];
echo "User ID-".$UserID."\n";
if($EstimatedRoom){
	while($row = mysqli_fetch_assoc($EstimatedRoom)){
		$eLocation= "Estimated Location is at ".$row['Area']." , ".$row['Building']." , ".$row['RoomName']." Grid Number ".$grid;
	}
		
}
echo $eLocation;
$assignLoc = "UPDATE Users SET Location = '$eLocation' WHERE ID_Number = '$UserID';";

$res=mysqli_query($connect,$assignLoc);

?>
